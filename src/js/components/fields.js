$(() => {
	// активация апруведа
	setTimeout(() => {
		$('.js-approved').addClass('is-visible');
	}, 500);
	// если поля не заполнены, то убираем апрувед
	$('.js-input').on('change', () => {
		if ($('.js-input').val().length < 1) {
			$('.js-approved').removeClass('is-visible');
		} else {
			$('.js-approved').addClass('is-visible');
		}
	});
	// определяем в фокусе ли элемент и отображаем это
	$('.js-input').on('focus', function () {
		$(this).siblings('.card-profile-top__label').addClass('focus');
	})
		.on('blur', function () {
			if ($(this).val().length <= 0) {
				$(this).siblings('.card-profile-top__label').removeClass('focus');
				if ($(this).attr('required')) {
					$(this).addClass('required');
				}
			}
		});
});
