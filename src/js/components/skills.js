$(() => {
	let count = 0;
	// общее кол-во чеков
	const total = $('.js-input-check').length;

	// считаем скилуху и выводим её в верстку
	function updateSkill() {
		count = $('.js-input-check:checked').length; // считаем кол-во выбранных
		count = count * 100 / total; // получаем их процент

		// берём старое число и обновляем его
		let from = $('.js-skill-counter').attr('data-from');

		$('.js-skill-counter').attr('data-from', count);
		// анимируем обновление числа
		$('.js-skill-counter').prop('Counter', from).animate({
			Counter: count,
		}, {
			duration: 1000,
			step(num) {
				// выводим значения
				num = Math.ceil(num);
				$('.meter-clock').css('transform', `rotateZ(${-90 + num * 180 / 100}deg)`);
				num = Math.ceil(num * 6.66);
				$(this).text(num);
			},
		});
	}

	updateSkill();
	// при изменении кол-ва чекбоксов изменяем скил
	$('.js-input-check').on('change', () => {
		updateSkill(this.checked ? 1 : -1);
	});
});
