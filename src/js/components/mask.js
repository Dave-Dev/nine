// маска для полей ввода
import Cleave from 'cleave.js';
require('cleave.js/dist/addons/cleave-phone.ru.js');

$(() => {
	// запуск маски для телефона
	$('body').on('focus', '.js-mask-phone', function () {
		let cleave = new Cleave(this, {
			prefix: '+7',
			delimiters: [' ', ' ', ' '],
			blocks: [2, 3, 3, 2, 2],
		});
	})
		.on('focus', '.js-mask-date', function () {
			let cleave = new Cleave(this, {
				date: true,
				datePattern: ['d', 'm', 'Y'],
			});
		});
	// убираем с поля для телефона любые символы кроме цифр
	$('.js-mask-phone, .js-mask-date').on('keydown', (e) => {
		if (e.key.length === 1 && e.key.match(/[^0-9'".]/)) {
			return false;
		}
	});
});
